#!/bin/bash
set -e

# install OpenCover
nuget install OpenCover -Version 4.6.519 -OutputDirectory ./OpenCover

# run tests.
for path in test/*; do
    str=$(echo $path | tr "/" "\n")
    projectName=$(echo $str | cut --delimiter " " --fields 2)
    projectName=${projectName/.Tests/""}

    (cd $path;
    "..\\..\\OpenCover\OpenCover.4.6.519\tools\OpenCover.Console.exe" \
    -oldstyle \
    -target:"C:\Program Files\dotnet\dotnet.exe" \
    -filter:"+[$projectName]* -[*.Tests]* -[*]*.Api.* -[*]*.Web.* -[xunit.*]*" \
    -hideskipped:All \
    -mergebyhash \
    -excludebyattribute:"*.ExcludeFromCodeCoverage*" \
    -targetargs:"test" \
    -returntargetcode \
    -skipautoprops \
    -mergeoutput -output:"..\\..\\coverage.xml" -register:user)
done

# send this report to codecov.io
pip install codecov
codecov -f ".\coverage.xml" -X gcov --token=$CODECOV_TOKEN

# check the code coverage percentage
if [[ "${DONT_CHECK_CODE_COVERAGE}" != "true" ]]; then
    # get coverage result from coverage.xml
    result=$(cat ./coverage.xml | grep -oPm1 '(?<=sequenceCoverage=")[\d]+')
    # if result is lower than expected, notify and exit from build by code 1.
    expected=80
    if [[ $result < $expected && ! ("$result" -eq "100") ]]; then 
        echo "Error: Code coverage is $result. It is lower then expected $expected"
        sh ./.build/src/slack-notifier.sh "code-coverage-failed" $result $expected 
        exit 1
    fi
fi